// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.service.authentication.dto;

import com.facebook.swift.codec.ThriftField;
import com.facebook.swift.codec.ThriftStruct;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shanqian.thrift.dto.ServiceStatusDTO;

/**
 * Created by lepujiu on 16/1/18.
 */
@ThriftStruct
public final class UserProfileDTO {
  @JsonIgnore
  private ServiceStatusDTO serviceStatusDTO;

  private Long userId;

  private String fullName;

  private String emailAddress;

  private String mobilePhone;

  private Integer gender;

  private String jobTitle;

  private Integer userStatus;

  private Long createdTime;

  private Long lastModifiedTime;

  private String extend;

  @ThriftField(1)
  public ServiceStatusDTO getServiceStatusDTO() {
    return serviceStatusDTO;
  }

  @ThriftField(2)
  public Long getUserId() {
    return userId;
  }

  @ThriftField(3)
  public String getFullName() {
    return fullName;
  }

  @ThriftField(4)
  public String getEmailAddress() {
    return emailAddress;
  }

  @ThriftField(5)
  public String getMobilePhone() {
    return mobilePhone;
  }

  @ThriftField(6)
  public Integer getGender() {
    return gender;
  }

  @ThriftField(7)
  public String getJobTitle() {
    return jobTitle;
  }

  @ThriftField(8)
  public Integer getUserStatus() {
    return userStatus;
  }

  @ThriftField(9)
  public Long getCreatedTime() {
    return createdTime;
  }

  @ThriftField(10)
  public Long getLastModifiedTime() {
    return lastModifiedTime;
  }

  @ThriftField(11)
  public String getExtend() {
    return extend;
  }

  @ThriftField
  public void setServiceStatusDTO(ServiceStatusDTO serviceStatusDTO) {
    this.serviceStatusDTO = serviceStatusDTO;
  }

  @ThriftField
  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @ThriftField
  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  @ThriftField
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  @ThriftField
  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }

  @ThriftField
  public void setGender(Integer gender) {
    this.gender = gender;
  }

  @ThriftField
  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  @ThriftField
  public void setUserStatus(Integer userStatus) {
    this.userStatus = userStatus;
  }

  @ThriftField
  public void setCreatedTime(Long createdTime) {
    this.createdTime = createdTime;
  }

  @ThriftField
  public void setLastModifiedTime(Long lastModifiedTime) {
    this.lastModifiedTime = lastModifiedTime;
  }

  @ThriftField
  public void setExtend(String extend) {
    this.extend = extend;
  }
}
