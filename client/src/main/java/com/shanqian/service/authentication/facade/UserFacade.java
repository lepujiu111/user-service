package com.shanqian.service.authentication.facade;

import com.facebook.swift.service.ThriftMethod;
import com.facebook.swift.service.ThriftService;
import com.shanqian.service.authentication.dto.UserProfileDTO;
import com.shanqian.thrift.dto.BooleanDTO;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/15
 */
@ThriftService
public interface UserFacade {
  /**
   * steps:
   * 1. add org 2. add user 3. add org-member
   * @param orgName
   * @param userName
   * @param mobile
   * @param password
   * @return
   */
  @ThriftMethod
  public BooleanDTO signUpWithMobile(String orgName, String userName, String mobile, String password);

  /**
   * 用手机号登陆
   * @param mobile
   * @param password
   * @return
   */
  @ThriftMethod
  public BooleanDTO loginWithMobile(String mobile, String password);

  @ThriftMethod
  public BooleanDTO changePassword(long userId, String currentPassword, String newPassword );

  @ThriftMethod
  public BooleanDTO deleteUserProfile(long userId);

  @ThriftMethod
  public BooleanDTO updateUserProfile(UserProfileDTO userProfileDTO);

  @ThriftMethod
  public UserProfileDTO getUserProfileByUserId(long userId);
}
