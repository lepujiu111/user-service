package com.shanqian.service.authentication.dao;

import com.shanqian.crm.service.auth.dao.UserProfileDao;
import com.shanqian.crm.service.auth.model.UserProfile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/16
 */
public class UserProfileDaoTest extends BaseTest {
  private Logger LOGGER = LoggerFactory.getLogger(UserProfileDaoTest.class);

  @Autowired
  private UserProfileDao userProfileDao;

  private UserProfile userProfile;

  @Before
  public void setUp() throws Exception {
    userProfile = new UserProfile();
    userProfile.setEncryptedPassword("test");
    userProfile.setFullName("test");
    userProfile.setEmailAddress("test@sqian.com");
    userProfile.setMobilePhone("111111");
    userProfile.setGender(1);
    userProfile.setJobTitle("manager");
    userProfile.setUserStatus(2);
  }

  @Test
  public void testInsertUserProfile() throws Exception {
    long userId = userProfileDao.insertUserProfile(userProfile);

    UserProfile afterInserted = userProfileDao.findByPrimaryKey(userId);
    LOGGER.info("after inserted: " + afterInserted);

    Assert.assertEquals("test", afterInserted.getFullName());
    Assert.assertEquals("test", afterInserted.getEncryptedPassword());
    Assert.assertEquals("test@sqian.com", afterInserted.getEmailAddress());
    Assert.assertEquals("111111", afterInserted.getMobilePhone());
    Assert.assertEquals(1, afterInserted.getGender().intValue());
    Assert.assertEquals("manager", afterInserted.getJobTitle());
    Assert.assertEquals(2, afterInserted.getUserStatus().intValue());

    UserProfile u = userProfileDao.findByMobilePhone("111111");
    Assert.assertNotNull(u);

    UserProfile activeUser = userProfileDao.findActiveUserProfileByMobilePhone("111111");
    Assert.assertNotNull(activeUser);

    List<UserProfile> uList = userProfileDao.listUserProfileByUserIds(Arrays.asList(userId));
    Assert.assertEquals(1, uList.size());

    afterInserted.setFullName("update");
    userProfileDao.updateByPrimaryKeySelective(afterInserted);

    UserProfile afterUpdated = userProfileDao.findByPrimaryKey(userId);
    Assert.assertEquals("update", afterUpdated.getFullName());

    userProfileDao.deleteUserProfileByUserId(userId);

    UserProfile afterDeleted = userProfileDao.findByPrimaryKey(userId);

    Assert.assertNull(afterDeleted);
  }
}