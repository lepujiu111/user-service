// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.service.authentication.dao;


import com.shanqian.crm.service.auth.UserServiceApplication;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = UserServiceApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:8811")
@Transactional(value = "transactionManager")
public class BaseTest {

  @Before
  public void init() {}
}
