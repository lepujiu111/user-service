// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.service.authentication.dao;

import com.shanqian.commons.service.enums.MemberStatus;
import com.shanqian.commons.service.enums.MemberType;
import com.shanqian.commons.service.utils.TimeUtils;
import com.shanqian.crm.service.auth.dao.OrgDao;
import com.shanqian.crm.service.auth.dao.OrgMemberDao;
import com.shanqian.crm.service.auth.model.Org;
import com.shanqian.crm.service.auth.model.OrgMember;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class OrgDaoTest extends BaseTest {

  private static Logger LOGGER = LoggerFactory.getLogger(OrgDaoTest.class);

  @Autowired
  OrgDao orgDao;

  @Autowired
  OrgMemberDao orgMemberDao;

  @Before
  public void init() {
  }

  /**
   * Test: 1) insertOrg() 2) findOrgByPrimaryKey()
   */
  @Test
  public void testInsertOrg() {

    // arg
    String orgName = "Brand New Ltd.";

    // work
    Org org = new Org();
    org.setOrgName(orgName);
    orgDao.insertOrg(org);

    // verify
    LOGGER.info("orgId=" + org.getOrgId());
    Org insertedOrg = orgDao.findOrgByPrimaryKey(org.getOrgId());
    Assert.assertEquals(orgName, insertedOrg.getOrgName());

  }

  @Test
  public void testDeleteOrgMember() {
    long userId = 1l;
    OrgMember o1 = new OrgMember();
    o1.setUserId(userId);
    o1.setOrgId(2l);
    o1.setMemberType(1);
    o1.setMemberStatus(2);
    o1.setLastActivatedTime(System.currentTimeMillis());
    orgMemberDao.insertOrgMember(o1);

    orgMemberDao.deleteOrgMemberByUserId(userId);
    List<Long> userIdList = orgMemberDao.listUserIdListByOrgId(2l);
    Assert.assertEquals(0, userIdList.size());
  }

  /**
   * Test: 1) findLastActivatedOrgByUserId
   */
  @Test
  public void testFindLastActivatedOrg() {

    // arg
    String orgName = "Brand New Ltd.";
    long userId = 1L;

    // work
    Org org = new Org();
    org.setOrgName(orgName);
    orgDao.insertOrg(org);
    long firstOrgId = org.getOrgId();

    orgDao.insertOrg(org);
    long secondOrgId = org.getOrgId();

    OrgMember orgMember = new OrgMember();
    orgMember.setUserId(userId);
    orgMember.setOrgId(firstOrgId);
    orgMember.setLastActivatedTime(TimeUtils.getNowTimestmapInMillis());
    orgMember.setMemberType(MemberType.OWNER.getCode());
    orgMember.setMemberStatus(MemberStatus.ACTIVE.getCode());
    orgMemberDao.insertOrgMember(orgMember);
    LOGGER.info("testFindLastActivatedOrg(): 1st orgMember=" + orgMember);

    orgMember.setOrgId(secondOrgId);
    orgMember.setLastActivatedTime(TimeUtils.getNowTimestmapInMillis() + 100);
    orgMemberDao.insertOrgMember(orgMember);
    LOGGER.info("testFindLastActivatedOrg(): 2nd orgMember=" + orgMember);

    // verify
    Org lastActivatedOrg = orgDao.findLastActivatedOrgByUserId(userId);
    LOGGER.info("testFindLastActivatedOrg(): lastActivatedOrg=" + lastActivatedOrg);
    Assert.assertEquals(secondOrgId, lastActivatedOrg.getOrgId().intValue());

  }

}
