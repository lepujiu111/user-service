package com.shanqian.service.authentication.service;

import com.shanqian.commons.service.utils.PasswordUtils;
import com.shanqian.crm.service.auth.dao.UserProfileDao;
import com.shanqian.crm.service.auth.model.UserProfile;
import com.shanqian.crm.service.auth.service.IProfileService;
import com.shanqian.crm.service.auth.service.IUserProfileService;
import com.shanqian.crm.service.auth.service.impl.UserProfileService;
import com.shanqian.service.authentication.dao.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/18
 */
public class UserProfileServiceTest extends BaseTest {

  private static Logger LOGGER = LoggerFactory.getLogger(UserProfileServiceTest.class);

  @Autowired
  IUserProfileService userProfileService;

  @Autowired
  UserProfileDao userProfileDao;

  @Test
  public void testSignUpWithMobile() throws Exception {
    String orgName = "shanqian";
    String fullName = "shanqian_test";
    String mobilePhone = "13567890012";
    String password = "Lpj52011111@@";

    // test sign up
    boolean result = userProfileService.signUpWithMobile(orgName, fullName, mobilePhone, password);
    Assert.assertTrue(result);

    // test login
    result = userProfileService.loginWithMobile(mobilePhone, password);
    Assert.assertTrue(result);
  }

  @Test
  public void testChangePassword() throws Exception {
    String orgName = "shanqian";
    String fullName = "shanqian_test";
    String mobilePhone = "13567890012";
    String password = "Lpj52011111@@";

    // test sign up
    boolean result = userProfileService.signUpWithMobile(orgName, fullName, mobilePhone, password);
    Assert.assertTrue(result);

    // test change password
    UserProfile userProfile = userProfileDao.findByMobilePhone(mobilePhone);
    String newPassword = "Lpj5201234@#";
    result = userProfileService.changePassword(userProfile.getUserId(), password, newPassword);
    Assert.assertTrue(result);

    // test login
    result = userProfileService.loginWithMobile(mobilePhone, newPassword);
    Assert.assertTrue(result);

    // test update
    String emailAddress = "test@sqian.com";
    userProfile.setEmailAddress(emailAddress);
    result = userProfileService.updateUserProfile(userProfile);
    Assert.assertTrue(result);
    userProfile = userProfileDao.findByMobilePhone(mobilePhone);
    Assert.assertEquals(emailAddress, userProfile.getEmailAddress());

    // test delete
    result = userProfileService.deleteUserProfile(userProfile.getUserId());
    Assert.assertTrue(result);
    userProfile = userProfileDao.findByMobilePhone(mobilePhone);
    Assert.assertNull(userProfile);
  }

  @Test
  public void testDeleteUserProfile() throws Exception {

  }

  @Test
  public void testUpdateUserProfile() throws Exception {

  }
}