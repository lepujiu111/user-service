package com.shanqian.crm.service.auth.service.impl;

import com.shanqian.crm.service.auth.service.IProfileService;
import org.springframework.stereotype.Service;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/19
 */
@Service("profileService")
public class ProfileService implements IProfileService {
  @Override
  public void test() {
    System.out.println("test");
  }
}
