// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Org {

  private Long orgId;

  private String orgName;

  private Long createdTime;

  private Long lastModifiedTime;

  private String extend;

}
