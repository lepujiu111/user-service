package com.shanqian.crm.service.auth.service;


import com.shanqian.crm.service.auth.model.UserProfile;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/17
 */
public interface IUserProfileService {
  boolean signUpWithMobile(String orgName, String userName, String mobilePhone, String password);

  boolean loginWithMobile(String mobilePhone, String password);

  boolean changePassword(long userId, String currentPassword, String newPassword);

  boolean deleteUserProfile(long userId);

  boolean updateUserProfile(UserProfile userProfile);

  UserProfile getUserProfileByUserId(long userId);
}
