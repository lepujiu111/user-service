// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.thrift.server;

import com.shanqian.service.authentication.facade.UserFacade;
import com.shanqian.thrift.server.SqThriftServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("userServer")
public class UserServer {

  @Autowired
  private UserFacade userFacade;

  @Value("${thrift.port.user}")
  private Integer port;

  @Value("${thrift.zkpath.user}")
  private String zkPath;

  @PostConstruct
  private void init() {
    SqThriftServer.start(userFacade, port, zkPath);
  }

}
