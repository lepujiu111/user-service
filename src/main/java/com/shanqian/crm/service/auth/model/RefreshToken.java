package com.shanqian.crm.service.auth.model;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/20
 */
public class RefreshToken {

  private Long userId;

  private String refreshToken;

  /**
   * 当前状态（0:inactive, 2:active）
   */
  private Integer status;
}
