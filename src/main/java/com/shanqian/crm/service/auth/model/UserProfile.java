// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class UserProfile implements Serializable {

  private Long userId;

  @JsonIgnore
  private String encryptedPassword;

  private String fullName;

  private String emailAddress;

  private String mobilePhone;

  // 1:femail 2:mail 0:not set
  private Integer gender;

  private String jobTitle;

  private Integer userStatus;

  private Long createdTime;

  private Long lastModifiedTime;

  private String extend;

  private Integer isDeleted;

}
