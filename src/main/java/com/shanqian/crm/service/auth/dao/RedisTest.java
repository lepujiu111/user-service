package com.shanqian.crm.service.auth.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/20
 */
@Repository("redisTest")
public class RedisTest {
  // inject the actual template
  @Autowired
  private RedisTemplate<String, String> template;

  // inject the template as ListOperations
  // can also inject as Value, Set, ZSet, and HashOperations
  @Resource(name="redisTemplate")
  private ValueOperations<String, String> valueOperations;

  public void addLink(String userId, String url) {
    // valueOperations.set(userId, url);
    valueOperations.set("q","q");
    System.out.println("test");
  }
}
