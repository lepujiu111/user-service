// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.dao;

import com.shanqian.crm.service.auth.model.UserProfile;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author: lepujiu
 * @Version: 1.0
 * @Created: 16/1/15
 */
@Repository("userProfileDao")
public class UserProfileDao {

  private static final String BASE_PACKAGE = "com.shanqian.service.authentication.dao.UserProfileMapper.";

  @Autowired
  SqlSessionTemplate sqlSessionTemplate;

  public long insertUserProfile(UserProfile userProfile) {
    sqlSessionTemplate.insert(BASE_PACKAGE + "insertUserProfile", userProfile);
    return userProfile.getUserId();
  }

  public void deleteUserProfileByUserId(long userId) {
    sqlSessionTemplate.update(BASE_PACKAGE + "deleteByPrimaryKey", userId);
  }

  public int updateByPrimaryKeySelective(UserProfile userProfile) {
    return sqlSessionTemplate.update(BASE_PACKAGE + "updateByPrimaryKeySelective", userProfile);
  }

  public UserProfile findByPrimaryKey(long userId) {
  return sqlSessionTemplate.selectOne(BASE_PACKAGE + "findByPrimaryKey", userId);
  }

  public UserProfile findActiveUserProfileByMobilePhone(String mobilePhone) {
    return sqlSessionTemplate.selectOne(BASE_PACKAGE + "findActiveUserProfileByMobilePhone", mobilePhone);
  }

  public UserProfile findByMobilePhone(String mobilePhone) {
    return sqlSessionTemplate.selectOne(BASE_PACKAGE  + "findByMobilePhone", mobilePhone);
  }

  public List<UserProfile> listUserProfileByUserIds(List<Long> userIds) {
    return sqlSessionTemplate.selectList(BASE_PACKAGE + "listUserProfileByUserIds", userIds);
  }

  public int resetOrChangePasswordByPrimaryKey(UserProfile userProfile) {
    return sqlSessionTemplate.update(BASE_PACKAGE + "resetPasswordByPrimaryKey", userProfile);
  }

}
