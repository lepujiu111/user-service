package com.shanqian.crm.service.auth;// Copyright (C) 2015 Shanqian
// All rights reserved

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@ImportResource("classpath:application-context.xml")
@ComponentScan(value = "com.shanqian.crm.service.auth")
@EnableTransactionManagement
public class UserServiceApplication {

  public static void main(String[] args) {

    SpringApplication.run(UserServiceApplication.class);
  }

}