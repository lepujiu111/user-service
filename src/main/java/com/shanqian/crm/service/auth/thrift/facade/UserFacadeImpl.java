// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.thrift.facade;

import com.shanqian.commons.service.enums.ServiceStatus;
import com.shanqian.crm.service.auth.dao.UserProfileDao;
import com.shanqian.crm.service.auth.helper.FacadeExceptionHelper;
import com.shanqian.crm.service.auth.model.UserProfile;
import com.shanqian.crm.service.auth.service.IUserProfileService;
import com.shanqian.service.authentication.dto.UserProfileDTO;
import com.shanqian.service.authentication.facade.UserFacade;
import com.shanqian.thrift.dto.BooleanDTO;
import com.shanqian.thrift.dto.ServiceStatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/15
 */
@Service("userFacadeImpl")
public class UserFacadeImpl implements UserFacade {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserFacadeImpl.class);

  @Autowired
  private IUserProfileService userProfileService;

  @Override
  public BooleanDTO signUpWithMobile(String orgName, String userName, String mobile, String password) {
    BooleanDTO result = new BooleanDTO();
    ServiceStatusDTO serviceStatusDTO =
            new ServiceStatusDTO(ServiceStatus.CREATED.getCode(), ServiceStatus.CREATED.getMsg());
    result.setServiceStatusDTO(serviceStatusDTO);

    try {
      boolean isSuccessful = userProfileService.signUpWithMobile(orgName, userName, mobile, password);
      result.setData(isSuccessful);
      LOGGER.info("sign up success: " + userName + " " + mobile);
    } catch (Exception e) {
      FacadeExceptionHelper.setServiceStatusForFacadeResult(serviceStatusDTO, e);
    }
    return result;
  }

  @Override
  public BooleanDTO loginWithMobile(String mobile, String password) {
    BooleanDTO result = new BooleanDTO();
    ServiceStatusDTO serviceStatusDTO =
            new ServiceStatusDTO(ServiceStatus.OK.getCode(), ServiceStatus.OK.getMsg());
    result.setServiceStatusDTO(serviceStatusDTO);

    try {
      boolean isSuccessful = userProfileService.loginWithMobile(mobile, password);
      result.setData(isSuccessful);
      LOGGER.info("login success: " + mobile);
    } catch (Exception e) {
      FacadeExceptionHelper.setServiceStatusForFacadeResult(serviceStatusDTO, e);
    }
    return result;
  }

  @Override
  public BooleanDTO changePassword(long userId, String currentPassword, String newPassword) {
    BooleanDTO result = new BooleanDTO();
    ServiceStatusDTO serviceStatusDTO =
            new ServiceStatusDTO(ServiceStatus.OK.getCode(), ServiceStatus.OK.getMsg());
    result.setServiceStatusDTO(serviceStatusDTO);

    try {
      boolean isSuccessful = userProfileService.changePassword(userId, currentPassword, newPassword);
      result.setData(isSuccessful);
    } catch (Exception e) {
      FacadeExceptionHelper.setServiceStatusForFacadeResult(serviceStatusDTO, e);
    }
    return result;
  }

  @Override
  public BooleanDTO deleteUserProfile(long userId) {
    BooleanDTO result = new BooleanDTO();
    ServiceStatusDTO serviceStatusDTO =
            new ServiceStatusDTO(ServiceStatus.OK.getCode(), ServiceStatus.OK.getMsg());
    result.setServiceStatusDTO(serviceStatusDTO);

    try {
      boolean isSuccessful = userProfileService.deleteUserProfile(userId);
      result.setData(isSuccessful);
    } catch (Exception e) {
      FacadeExceptionHelper.setServiceStatusForFacadeResult(serviceStatusDTO, e);
    }
    return result;
  }

  @Override
  public BooleanDTO updateUserProfile(UserProfileDTO userProfileDTO) {
    BooleanDTO result = new BooleanDTO();
    ServiceStatusDTO serviceStatusDTO =
            new ServiceStatusDTO(ServiceStatus.OK.getCode(), ServiceStatus.OK.getMsg());
    result.setServiceStatusDTO(serviceStatusDTO);

    try {
      UserProfile userProfile = new UserProfile();
      BeanUtils.copyProperties(userProfileDTO, userProfile);
      boolean isSuccessufl = userProfileService.updateUserProfile(userProfile);
      result.setData(isSuccessufl);
    } catch (Exception e) {
      FacadeExceptionHelper.setServiceStatusForFacadeResult(serviceStatusDTO, e);
    }

    return result;
  }

  @Override
  public UserProfileDTO getUserProfileByUserId(long userId) {
    UserProfileDTO result = new UserProfileDTO();
    ServiceStatusDTO serviceStatusDTO =
            new ServiceStatusDTO(ServiceStatus.OK.getCode(), ServiceStatus.OK.getMsg());
    result.setServiceStatusDTO(serviceStatusDTO);

    try {
      UserProfile userProfile = userProfileService.getUserProfileByUserId(userId);
      BeanUtils.copyProperties(userProfile, result);
    } catch (Exception e) {
      FacadeExceptionHelper.setServiceStatusForFacadeResult(serviceStatusDTO, e);
    }

    return result;
  }
}

