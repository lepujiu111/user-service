package com.shanqian.crm.service.auth.service.impl;

import com.shanqian.commons.service.enums.MemberStatus;
import com.shanqian.commons.service.enums.MemberType;
import com.shanqian.commons.service.enums.ServiceStatus;
import com.shanqian.commons.service.enums.UserStatus;
import com.shanqian.commons.service.exceptions.ServiceStatusException;
import com.shanqian.commons.service.utils.IntegerUtils;
import com.shanqian.commons.service.utils.PasswordUtils;
import com.shanqian.commons.service.utils.PhoneUtils;
import com.shanqian.commons.service.utils.StringUtils;
import com.shanqian.crm.service.auth.dao.OrgDao;
import com.shanqian.crm.service.auth.dao.OrgMemberDao;
import com.shanqian.crm.service.auth.dao.UserProfileDao;
import com.shanqian.crm.service.auth.helper.UserProfileHelper;
import com.shanqian.crm.service.auth.model.Org;
import com.shanqian.crm.service.auth.model.OrgMember;
import com.shanqian.crm.service.auth.model.UserProfile;
import com.shanqian.crm.service.auth.service.IUserProfileService;
import com.shanqian.utils.logging.LogAround;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/18
 */
@Service("userProfileService")
public class UserProfileService implements IUserProfileService {
  private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileService.class);

  @Autowired
  UserProfileDao userProfileDao;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Autowired
  OrgDao orgDao;

  @Autowired
  OrgMemberDao orgMemberDao;

  @Override
  @LogAround
  @Transactional(value = "transactionManager", rollbackFor = Exception.class)
  public boolean signUpWithMobile(String orgName, String userName, String mobilePhone, String password) {
    UserProfileHelper.signupParamsCheck(userName, password, mobilePhone);

    UserProfile existingUserProfile= userProfileDao.findByMobilePhone(mobilePhone);
    if (null != existingUserProfile
            && IntegerUtils.equals(existingUserProfile.getUserStatus(), UserStatus.ACTIVE.getCode())) {
      throw new ServiceStatusException(ServiceStatus.EXISTING_MOBILE);
    }

    try {
      // add user
      UserProfile userProfile = new UserProfile();
      userProfile.setEncryptedPassword(passwordEncoder.encode(password));
      userProfile.setFullName(userName);
      userProfile.setEmailAddress("");
      userProfile.setMobilePhone(mobilePhone);
      userProfile.setGender(0);
      userProfile.setJobTitle("");
      userProfile.setUserStatus(UserStatus.ACTIVE.getCode());
      long userId = userProfileDao.insertUserProfile(userProfile);

      // add org
      Org org = new Org();
      org.setOrgName(orgName);
      long orgId = orgDao.insertOrg(org);

      // add org-member
      OrgMember orgMember = new OrgMember();
      orgMember.setUserId(userId);
      orgMember.setOrgId(orgId);
      orgMember.setMemberType(MemberType.OWNER.getCode());
      orgMember.setMemberStatus(MemberStatus.ACTIVE.getCode());
      orgMember.setLastActivatedTime(System.currentTimeMillis());
      orgMemberDao.insertOrgMember(orgMember);
    }catch (Exception e) {
      LOGGER.info("signupByMobile(): internal error", e);
      throw new ServiceStatusException(ServiceStatus.INTERNAL_SERVER_ERROR);
    }
    return true;
  }

  @Override
  @LogAround
  @Transactional(value = "transactionManager", rollbackFor = Exception.class)
  public boolean loginWithMobile(String mobilePhone, String password) {
    if (!PhoneUtils.isValidMobileNumber(mobilePhone)) {
      throw new ServiceStatusException(ServiceStatus.INVALID_PARAMS);
    }
    UserProfile userProfile = userProfileDao.findByMobilePhone(mobilePhone);
    if (null == userProfile) {
      throw new ServiceStatusException(ServiceStatus.NOT_FOUND);
    }
    String passwordInDb = userProfile.getEncryptedPassword();
    String providedPassword = passwordEncoder.encode(password);
    if (!passwordEncoder.matches(password, passwordInDb)) {
      throw new ServiceStatusException(ServiceStatus.INVALID_LOGIN_CREDENTIALS);
    }
    return true;
  }

  @Override
  @LogAround
  @Transactional(value = "transactionManager", rollbackFor = Exception.class)
  public boolean changePassword(long userId, String currentPassword, String newPassword) {
    if (!PasswordUtils.isValidPassword(newPassword)
            || !PasswordUtils.isValidPassword(currentPassword)
            || currentPassword.equals(newPassword)) {
      throw new ServiceStatusException(ServiceStatus.INVALID_PARAMS);
    }
    UserProfile userProfile = userProfileDao.findByPrimaryKey(userId);

    if (null == userProfile) {
      throw new ServiceStatusException(ServiceStatus.NOT_FOUND);
    }
    if (!passwordEncoder.matches(currentPassword, userProfile.getEncryptedPassword())) {
      throw new ServiceStatusException(ServiceStatus.INVALID_LOGIN_CREDENTIALS);
    }
    userProfile.setEncryptedPassword(passwordEncoder.encode(newPassword));
    try {
      userProfileDao.resetOrChangePasswordByPrimaryKey(userProfile);
    } catch (Exception e) {
      throw new ServiceStatusException(ServiceStatus.INTERNAL_SERVER_ERROR);
    }
    return true;
  }

  @Override
  @LogAround
  @Transactional(value = "transactionManager", rollbackFor = Exception.class)
  public boolean deleteUserProfile(long userId) {
    try {
      userProfileDao.deleteUserProfileByUserId(userId);
      orgMemberDao.deleteOrgMemberByUserId(userId);
    } catch (Exception e) {
      throw new ServiceStatusException(ServiceStatus.INTERNAL_SERVER_ERROR);
    }
    return true;
  }

  @Override
  @LogAround
  @Transactional(value = "transactionManager", rollbackFor = Exception.class)
  public boolean updateUserProfile(UserProfile userProfile) {
    if (!UserProfileHelper.isAcceptableUpdateRequest(userProfile)) {
      throw new ServiceStatusException(ServiceStatus.INVALID_PARAMS);
    }
    try {
      userProfileDao.updateByPrimaryKeySelective(userProfile);
    } catch (Exception e) {
      throw new ServiceStatusException(ServiceStatus.INTERNAL_SERVER_ERROR);
    }
    return true;
  }

  @Override
  public UserProfile getUserProfileByUserId(long userId) {
    UserProfile userProfile = userProfileDao.findByPrimaryKey(userId);
    if (null == userProfile) {
      throw new ServiceStatusException(ServiceStatus.NOT_FOUND);
    }
    return userProfile;
  }
}
