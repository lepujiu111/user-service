// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.helper;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.shanqian.commons.service.consts.JWTConsts;
import com.shanqian.commons.service.enums.ServiceStatus;
import com.shanqian.commons.service.exceptions.ServiceStatusException;
import com.shanqian.commons.service.utils.KeyUtils;
import com.shanqian.commons.service.utils.TimeUtils;
import com.shanqian.utils.codec.EncryptUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;

@Service("tokenHelper")
public class TokenHelper {

  private static final Logger LOGGER = LoggerFactory.getLogger(TokenHelper.class);

  private static final long REMEMBER_ME_TOKEN_LIFETIME_IN_MILLIS = 1000 * 60 * 60 * 24 * 7;
  private static final long NOT_REMEMBER_ME_TOKEN_LIFETIME_IN_MILLIS = 1000 * 60 * 60 * 2;
  private static final String FILE_PATH_PRIVATE_KEY = "keypair/AccessTokenPrivateRecipeDer";
  private static final String FILE_PATH_PUBLIC_KEY = "keypair/AccessTokenPublicRecipeDer";
  private static final String ISSUEER = "com.shanqian";

  private JWSHeader jwsHeader;

  private RSAPrivateKey rsaPrivateKey;

  private RSAPublicKey accessTokenPublicKey;

  private JWSVerifier jwsVerifier;

  private JWSSigner signer;

  // TODO: get secret key from ZK by orgId, probably

  @PostConstruct
  public void init() {
    try {
      rsaPrivateKey = KeyUtils.loadRsaPrivateKey(FILE_PATH_PRIVATE_KEY);
      accessTokenPublicKey = KeyUtils.loadRsaPublicKey(FILE_PATH_PUBLIC_KEY);
      signer = new RSASSASigner(rsaPrivateKey);
      jwsHeader = new JWSHeader(JWSAlgorithm.RS256);
      jwsVerifier = new RSASSAVerifier(accessTokenPublicKey);
    } catch (Exception e) {
      LOGGER.error("init(): Fatal! fail to init key or signer", e);
      throw new ServiceStatusException(ServiceStatus.INTERNAL_SERVER_ERROR);
    }
  }

  public String generateAccessToken(long userId, long orgId, boolean allowedRememberMe) throws Exception {

    String accessToken = null;
    String encryptedUserId = EncryptUtils.symmetricEncrypt(String.valueOf(userId));
    String encryptedOrgId = EncryptUtils.symmetricEncrypt(String.valueOf(orgId));
    long now = TimeUtils.getNowTimestmapInMillis();
    long lifetime = (allowedRememberMe ? REMEMBER_ME_TOKEN_LIFETIME_IN_MILLIS
                                       : NOT_REMEMBER_ME_TOKEN_LIFETIME_IN_MILLIS);
    Date expireAt = new Date(now + lifetime);

    JWTClaimsSet claimsSet = new JWTClaimsSet();
    claimsSet.setCustomClaim(JWTConsts.CLAIM_NAME_USERID, encryptedUserId);
    claimsSet.setCustomClaim(JWTConsts.CLAIM_NAME_ORGID, encryptedOrgId);
    claimsSet.setIssuer(ISSUEER);
    claimsSet.setIssueTime(new Date(now));
    claimsSet.setExpirationTime(expireAt);
    SignedJWT signedJWT = new SignedJWT(jwsHeader, claimsSet);
    signedJWT.sign(signer);
    accessToken = signedJWT.serialize();

    return accessToken;
  }

}
