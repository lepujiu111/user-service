package com.shanqian.crm.service.auth.helper;

import com.shanqian.commons.service.enums.ServiceStatus;
import com.shanqian.commons.service.exceptions.ServiceStatusException;
import com.shanqian.commons.service.utils.PasswordUtils;
import com.shanqian.commons.service.utils.PhoneUtils;
import com.shanqian.commons.service.utils.StringUtils;
import com.shanqian.crm.service.auth.model.UserProfile;

/**
 * @author lepujiu
 * @version 1.0
 * @created 16/1/18
 */
public class UserProfileHelper {
  public static boolean isAcceptableUpdateRequest(UserProfile userProfile) {
    if (null == userProfile
            || (null == userProfile.getFullName() || !StringUtils.isValidVarchar100(userProfile.getFullName()))) {
      return false;
    }
    return true;
  }

  public static void signupParamsCheck(String userName, String password, String mobilePhone) {
    if (!StringUtils.isValidVarchar100(userName)) {
      throw new ServiceStatusException(ServiceStatus.INVALID_PARAMS, "fullName is not valid");
    }
    if (!PasswordUtils.isValidPassword(password)) {
      throw new ServiceStatusException(ServiceStatus.INVALID_PARAMS, "password is not valid");
    }
    if (!PhoneUtils.isValidMobileNumber(mobilePhone)) {
      throw new ServiceStatusException(ServiceStatus.INVALID_PARAMS, "mobile phone is not valid");
    }
  }
}
