// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.helper;

import com.shanqian.commons.service.enums.ServiceStatus;
import com.shanqian.commons.service.exceptions.ServiceStatusException;
import com.shanqian.thrift.dto.ServiceStatusDTO;

/**
 * @Author: Zhe Chen
 * @Version: 1.0
 * @Created: 2015-12-24
 */
public class FacadeExceptionHelper {

  public static void setServiceStatusForFacadeResult(ServiceStatusDTO serviceStatusDTO, Exception e) {

    // TEST
    System.out.println("in exp handler, e=" + e);

    ServiceStatus serviceStatus = null;
    if (e instanceof ServiceStatusException) {
      ServiceStatusException serviceStatusException = (ServiceStatusException) e;
      serviceStatus = serviceStatusException.getServiceStatus();
    } else {
     serviceStatus = ServiceStatus.INTERNAL_SERVER_ERROR;
    }

    serviceStatusDTO.setCodeAndMessage(serviceStatus.getCode(), serviceStatus.getMsg());
  }

}
