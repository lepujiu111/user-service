// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrgMember {

  private Long orgMemberId;

  private Long userId;

  private Long orgId;

  private Integer memberType;

  private Integer memberStatus;

  private Long createdTime;

  private Long lastModifiedTime;

  private Long lastActivatedTime;

  private String extend;

  private Integer isDeleted;

}
