// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.dao;

import com.shanqian.crm.service.auth.model.Org;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("orgDao")
public class OrgDao {

  private static final String BASE_PACKAGE = "com.shanqian.service.authentication.dao.OrgMapper.";

  @Autowired
  SqlSessionTemplate sqlSessionTemplate;

  public long insertOrg(Org org) {
    sqlSessionTemplate.insert(BASE_PACKAGE + "insertOrg", org);
    return org.getOrgId();
  }

  public Org findOrgByPrimaryKey(long orgId) {
    return sqlSessionTemplate.selectOne(BASE_PACKAGE + "findOrgByPrimaryKey", orgId);
  }

  public Org findLastActivatedOrgByUserId(long userId) {
    return sqlSessionTemplate.selectOne(BASE_PACKAGE + "findLastActivatedOrgByUserId", userId);
  }

}
