// Copyright (C) 2015 Shanqian
// All rights reserved

package com.shanqian.crm.service.auth.dao;

import com.shanqian.crm.service.auth.model.OrgMember;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository("orgMemberDao")
public class OrgMemberDao {

  private static final String BASE_PACKAGE = "com.shanqian.service.authentication.dao.OrgMemberMapper.";

  @Autowired
  SqlSessionTemplate sqlSessionTemplate;

  public long insertOrgMember(OrgMember orgMember) {
    sqlSessionTemplate.insert(BASE_PACKAGE + "insertOrgMember", orgMember);
    return orgMember.getOrgMemberId();
  }

  public List<Long> listUserIdListByOrgId(long orgId) {
    List<Long> idList = sqlSessionTemplate.selectList(BASE_PACKAGE + "listUserIdListByOrgId", orgId);
    if (idList.size() == 0) {
      idList = Collections.EMPTY_LIST;
    }
    return idList;
  }

  public void deleteOrgMemberByUserId(long userId) {
    sqlSessionTemplate.update(BASE_PACKAGE + "deleteOrgMemberByUserId", userId);
  }

  public long updateOrgMember(OrgMember orgMember) {
    return sqlSessionTemplate.update("updateOrgMember", orgMember);
  }

}
